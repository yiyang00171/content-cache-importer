<?php

use Exception\DirectoryNotExist;
use Exception\DirectoryNotReadable;
use Service\Importer;
use Service\MessageBuilder;
use Service\PathBuilder;

require_once 'config/config.php';
require_once 'autoload.php';

$userManual = function() {
    return <<<EOM
Prerequisite:
    Database should be initiated using config/db-init.sql.
    Database HOST should be defined in config/config.php.

Usage:
    - Import content types for all builds:
        php ./main.php
    - Import content types for specific builds:
        php ./main.php -b {build-name-1} [-b {build-name-2}] [-b {build-name-3}] ...
    - Import content types except for specific builds:
        php ./main.php -d {build-name-1} [-d {build-name-2}] [-d {build-name-3}] ...
EOM;
};

$args = getopt('b:d:h');
if (isset($args['h'])) {
    echo $userManual();
    exit;
}

// If build name is specified, script will only import answer files under the given build
$builds = isset($args['b']) ? (array) $args['b'] : array();
$excludedBuilds = isset($args['d']) ? (array) $args['d'] : array();


// Scan all builds to get file directories
$modulePaths = PathBuilder::findModulePaths($moduleDirs, $builds, $excludedBuilds);
$importer = new Importer();

foreach ($modulePaths as $modulePath) {
    try {
        $importer->import($modulePath);
    }
    catch (DirectoryNotExist $e) {
        echo MessageBuilder::buildError($e->getMessage());
    }
    catch (DirectoryNotReadable $e) {
        echo MessageBuilder::buildError($e->getMessage());
    }
}


