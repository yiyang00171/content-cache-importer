<?php

namespace Database;

use PDO;
use PDOException;

class ContentType {

    const TABLE_NAME = 'content_types';

    /**
     * @var PDO
     */
    private $pdo;

    private static $instance;

    /**
     * @throws PDOException
     */
    private function __construct() {
        $dns = sprintf('mysql:dbname=%s;host=%s', DB_NAME, HOST);

        $this->pdo = new PDO($dns, USER, PASS, array(
            PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'
        ));
    }

    public static function getInstance() {
        if (!self::$instance) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    /**
     * @param string $path
     * @param string $contentType
     * @return bool
     */
    public function save($path, $contentType) {
        $sql = sprintf('REPLACE INTO %s (path, content_type) VALUES (?, ?)', self::TABLE_NAME);
        $stmt = $this->pdo->prepare($sql);
        return $stmt->execute(array($path, $contentType));
    }

}