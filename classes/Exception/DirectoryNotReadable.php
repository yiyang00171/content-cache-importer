<?php

namespace Exception;

use RuntimeException;

class DirectoryNotReadable extends RuntimeException {

}