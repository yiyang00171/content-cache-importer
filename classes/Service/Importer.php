<?php

namespace Service;

use Database\ContentType;
use Exception\DirectoryNotExist;
use Exception\DirectoryNotReadable;

class Importer {

    public function import($dir) {
        if (!is_dir($dir) ) {
            throw new DirectoryNotExist(sprintf('Directory %s does not exist', $dir));
        }

        if (!is_readable($dir)) {
            throw new DirectoryNotReadable(sprintf('Directory %s is not readable', $dir));
        }

        $files = $this->getFiles($dir);
        $message = sprintf('Number of files under directory %s: %d', $dir, count($files));
        echo MessageBuilder::buildHighlight($message);

        foreach ($files as $file) {
            // Get content type
            $path = realpath($file);
            $contentType = $this->getContentType($path);

            // Save content type
            if (ContentType::getInstance()->save($path, $contentType)) {
                $message = sprintf('Content type saved. File: %s, Content Type: %s', $path, $contentType);
                echo MessageBuilder::buildInfo($message);
            }
            else {
                $message = sprintf('Failed to save content type for file: %s', $path);
                echo MessageBuilder::buildError($message);
            }
        }

    }

    private function getFiles($dir) {
        $files = array();
        if ($items = scandir($dir)) {
            foreach ($items as $item) {
                $itemPath = $dir . '/' . $item;
                if ($item != '.' && $item != '..' && $item != '.keep' && is_file($itemPath)) {
                    $files[] = $itemPath;
                }
            }
        }
        return $files;
    }

    private function getContentType($path) {
        $contentType = null;
        $predefinedTypes = array(
            'mp4' => 'video/mp4',
            'pdf' => 'application/pdf',
            'docx' => 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
            'pptx' => 'application/vnd.openxmlformats-officedocument.presentationml.presentation',
            'ppsx' => 'application/vnd.openxmlformats-officedocument.presentationml.slideshow',
            'xlsx' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            'eml' => 'message/rfc822'
        );

        $extension = strtolower(pathinfo($path, PATHINFO_EXTENSION));
        if (isset($predefinedTypes[$extension])) {
            $contentType = $predefinedTypes[$extension];
        }
        else {
            $contentType = exec(sprintf('file --mime-type -b %s', $path));
        }

        return $contentType;
    }

}