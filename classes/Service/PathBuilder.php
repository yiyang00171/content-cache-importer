<?php

namespace Service;

class PathBuilder {

    /**
     * @param array $moduleDirs
     * @param array $builds, if specified method will search through given builds
     * @param array $excludedBuilds, if specified method will excluded given builds
     * @return array
     */
    public static function findModulePaths(array $moduleDirs, array $builds = array(), array $excludedBuilds = array()) {
        if (!$builds) {
            // Scan all builds if not specified
            $items = scandir(BUILD_PATH);
            foreach ($items as $item) {
                if ($item != '.' && $item != '..' && $item != '.keep' && is_dir(BUILD_PATH . '/' . $item)) {
                    $builds[] = $item;
                }
            }
        }

        $paths = array();
        foreach ($builds as $build) {
            if (!in_array($build, $excludedBuilds)) {
                $paths = array_merge($paths, self::findModulePathsByBuild($moduleDirs, $build));
            }
        }

        return $paths;
    }

    /**
     * @param array $moduleDirs
     * @param string $build
     * @return array
     */
    private static function findModulePathsByBuild(array $moduleDirs, $build) {
        $modulePaths = array();
        foreach ($moduleDirs as $dir) {
            $parts = array(
                BUILD_PATH,
                $build,
                CONTENT_DIR,
                QUESTION_FILE_DIR,
                $dir
            );
            $modulePaths[] = implode('/', $parts);
        }

        return $modulePaths;
    }

}