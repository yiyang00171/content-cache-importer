<?php

namespace Service;

class MessageBuilder {

    public static function buildInfo($message) {
        return sprintf("\033[0;32m %s\033[0m\n", $message);
    }

    public static function buildHighlight($message) {
        return sprintf("\033[1;36m %s\033[0m\n", $message);
    }

    public static function buildError($message) {
        return sprintf("\033[1;31m ERROR: %s\033[0m\n", $message);
    }

}