<?php

spl_autoload_register(function ($class) {
    require_once 'classes/' . str_replace('\\', '/', $class) . '.php';
});