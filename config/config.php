<?php

// DB
define('HOST', ''); // TODO: define the database host
define('DB_NAME', 'wm_content_types');
define('USER', 'content_types');
define('PASS', 'content_types');

// CONTENT DIR
define('BUILD_PATH', '/var/www/builds');
define('CONTENT_DIR', 'content');
define('QUESTION_FILE_DIR', 'questionsFileUpload');

// Module DIR
define('DIR_APPRAISAL', 'appraisals');
define('DIR_EXIT_INTERVIEW', 'exitInterviews');
define('DIR_EXTERNAL_SURVEY', 'externalSurveys');
define('DIR_CUSTOM_FEEDBACK', 'feedback');
define('DIR_INCIDENT_REPORT', 'incidents');
define('DIR_INTERNAL_SURVEY', 'internalSurveys');
define('DIR_RECRUITMENT', 'recruitment');
define('DIR_LOCATION_REVIEW', 'reviews');

// Modules to be imported
$moduleDirs = array(
    DIR_APPRAISAL,
    DIR_EXIT_INTERVIEW,
    DIR_EXTERNAL_SURVEY,
    DIR_CUSTOM_FEEDBACK,
    DIR_INCIDENT_REPORT,
    DIR_INTERNAL_SURVEY,
    DIR_RECRUITMENT,
    DIR_LOCATION_REVIEW
);
