CREATE DATABASE wm_content_types;

CREATE USER 'content_types'@'%' IDENTIFIED BY 'content_types';

GRANT ALL PRIVILEGES ON wm_content_types.* TO 'content_types'@'%';

FLUSH PRIVILEGES;

USE wm_content_types;

CREATE TABLE content_types (
    id INT(10) UNSIGNED AUTO_INCREMENT,
    path VARCHAR(250),
    content_type VARCHAR(100),
    PRIMARY KEY (id),
    UNIQUE KEY (path)
) DEFAULT CHARACTER SET utf8;